package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

var data []uint16
var dataBytes []byte
var err error

func main() {
	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			dataBytes = append(dataBytes, scanner.Bytes()...)
		}
		err = scanner.Err()
		er(err)
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "help" || arg == "-h" {
			fmt.Print("-h | --help\tPrint this help message\n")
			fmt.Print("-f | --file\tFile to calculate checksum (Defaut: STDIN)\n")
			return
		}

		if (arg == "-f" || arg == "--file") && !isPipe() && n+1 < len(args) {
			filename := args[n+1]
			_, err = os.Stat(filename)
			er(err)

			fh, err := os.Open(filename)
			er(err)
			defer fh.Close()

			scanner := bufio.NewScanner(fh)
			for scanner.Scan() {
				dataBytes = append(dataBytes, scanner.Bytes()...)
			}
		}

	}

	if len(dataBytes) == 0 {
		er(fmt.Errorf("no data provided"))
	}

	for i := 0; i < len(dataBytes); i++ {
		if i%2 == 0 {
			if i+1 < len(dataBytes) {
				data = append(data, uint16(dataBytes[i])<<8+uint16(dataBytes[i+1]))
			} else {
				data = append(data, uint16(dataBytes[i])<<8)
			}
		}
	}

	var dataSum uint64
	for i := 0; i < len(data); i++ {
		dataSum = dataSum + uint64(data[i])
	}
	carry := dataSum >> 16

	dataSum = dataSum + carry

	result := ^uint16(dataSum)

	fmt.Printf("\n%16b - %4X\n", result, result)

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
